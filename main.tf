resource "azurerm_resource_group" "rg" {
  name     = "rg-vm-linux-${var.name}"
  location = var.location
  tags     = var.tags
}

data "azurerm_client_config" "current" {}

module "network-security-rules" {
  source   = "gitlab.com/operator-ict/network-security-rules/azurerm"
  version  = "0.1.0"

  name                                  = var.name
  rg_name                               = azurerm_resource_group.rg.name
  rg_location                           = azurerm_resource_group.rg.location
  default_destination_address_prefixes  = tolist([azurerm_network_interface.nic.private_ip_address])
  security_rules                        = var.security_rules
}

data "azurerm_public_ip" "public" {
    count = var.public_ip != null ? 1 : 0
    name = var.public_ip
    resource_group_name = var.public_ip_resource_group
}

resource "azurerm_network_interface" "nic" {
  name                = "${var.name}-nic"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Dynamic"

    # Only include the public IP configur]ation if the variable is set
    public_ip_address_id = var.public_ip != null ? data.azurerm_public_ip.public[0].id : null

  }
}

resource "random_password" "admin_pass" {
  length = 16
  special = false
}

resource "azurerm_key_vault" "kv" {
  name                            = "${var.name}"
  location                        = azurerm_resource_group.rg.location
  resource_group_name             = azurerm_resource_group.rg.name
  sku_name                        = "standard"
  tenant_id                       = data.azurerm_client_config.current.tenant_id
  enabled_for_template_deployment = true
  enable_rbac_authorization       = true
  soft_delete_retention_days      = 30
  purge_protection_enabled        = false
  tags                            = var.tags
}

resource "azurerm_key_vault_secret" "admin_pass" {
  key_vault_id = azurerm_key_vault.kv.id
  name         = "admin-pass"
  value        = random_password.admin_pass.result
}

resource "azurerm_windows_virtual_machine" "vm-win" {
  name                = var.name
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  size                = var.sku
  admin_username      = var.admin_username
  admin_password      = random_password.admin_pass.result
  network_interface_ids = [
    azurerm_network_interface.nic.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    disk_size_gb         = var.data_disk_size
  }

  source_image_reference {
    publisher = "microsoftwindowsdesktop"
    offer     = "windows-11"
    sku       = "win11-22h2-pro"
    version   = "latest"
  }
}

resource "azurerm_backup_protected_vm" "vm" {
  count               = var.backup_vm ? 1:0
  resource_group_name = var.backup_rg
  recovery_vault_name = var.recovery_vault_name
  source_vm_id        = azurerm_windows_virtual_machine.vm-win.id
  backup_policy_id    = var.backup_policy_id[var.policy_name]
}
