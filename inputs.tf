variable "name" {
  type = string
}

variable "location" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "subnet_id" {
  type = string
}

variable "sku" {
  type = string
}

variable "admin_username" {
  type = string
  default = "spravce"
}

variable "data_disk_size" {
  type = number
  default = 32
}

variable "public_ip_resource_group" {
  type = string
}

variable "backup_vm" {
  type = bool
  default = false
}

variable "recovery_vault_name" {
  type = string
  default = null
}

variable "backup_policy_id" {
  type = map(string)
  default = {}
}

variable "backup_rg" {
  type = string
  default = null
}

variable "policy_name" {
  type = string
  default = null
}

variable "public_ip" {
  type =  string
  default = null
}

variable "security_rules" {
  type = map(object({
    priority                      = number
    destination_address_prefixes  = optional(list(string),["127.0.0.1"])
    destination_port_ranges       = list(string)
    access                        = optional(string, "Allow")
    source_address_prefixes       = optional(list(string), ["Internet"])
    source_port_range             = optional(string, "*")
    direction                     = optional(string, "Inbound")
    protocol                      = optional(string, "Tcp")
  })
  )
  default = {}
}
